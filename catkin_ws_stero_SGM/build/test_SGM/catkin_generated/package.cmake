set(_CATKIN_CURRENT_PACKAGE "test_SGM")
set(test_SGM_VERSION "0.1.0")
set(test_SGM_MAINTAINER "XK Chen <Chen@todo.todo>")
set(test_SGM_PACKAGE_FORMAT "2")
set(test_SGM_BUILD_DEPENDS "roscpp rospy std_msgs")
set(test_SGM_BUILD_EXPORT_DEPENDS "roscpp rospy std_msgs")
set(test_SGM_BUILDTOOL_DEPENDS "catkin")
set(test_SGM_BUILDTOOL_EXPORT_DEPENDS )
set(test_SGM_EXEC_DEPENDS "roscpp rospy std_msgs")
set(test_SGM_RUN_DEPENDS "roscpp rospy std_msgs")
set(test_SGM_TEST_DEPENDS )
set(test_SGM_DOC_DEPENDS )
set(test_SGM_URL_WEBSITE "")
set(test_SGM_URL_BUGTRACKER "")
set(test_SGM_URL_REPOSITORY "")
set(test_SGM_DEPRECATED "")