catkin_ws_stereo_SGM:基于libSGM和ZED相机实现实时双目立体视觉匹配，用于深度信息的获取

1、软硬件依赖：
	1)、ZED相机、ZED相机的LinuxSDK以及ZED-ROS软件包
	2)、英伟达GPU以及CUDA
	3)、ROS
	4)、OpenCV
	5)、安装好基于CUDA加速的SGM半全局匹配算法软件包libSGM

2、主要实现内容：基于ROS分布式topic通信机制，订阅/zed/zed_node/left_raw/image_raw_gray及/zed/zed_node/right_raw/image_raw_gray两个图像话题作为输入，在话题订阅回调函数中将图像数据转换为单通道CV_8U格式并使用libSGM进行处理生成单通道深度图，给深度图上色转换为3通道BGR8格式图片，再使用publisher发布到图像话题/depth/SGM中

3、性能：在libSGM使用CUDA加速的情况下，两张1280×720的图像匹配时间为10ms左右，FPS稳定在80以上
