/*
Copyright 2016 Fixstars Corporation

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http ://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <iostream>
#include <iomanip>
#include <string>
#include <chrono>

#include <cuda_runtime.h>
#include <opencv4/opencv2/core/core.hpp>
#include <opencv4/opencv2/highgui/highgui.hpp>
#include <opencv4/opencv2/imgproc/imgproc.hpp>
#include <opencv4/opencv2/core/version.hpp>
#include <cv_bridge/cv_bridge.h>

#include <libsgm.h>

#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <ros/spinner.h>
#include <sensor_msgs/CameraInfo.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

#define ASSERT_MSG(expr, msg) \
    if (!(expr)) { \
        std::cerr << msg << std::endl; \
        std::exit(EXIT_FAILURE); \
    } \

struct device_buffer
{
    device_buffer() : data(nullptr) {}
    device_buffer(size_t count) { allocate(count); }
    void allocate(size_t count) { cudaMalloc(&data, count); }
    ~device_buffer() { cudaFree(data); }
    void* data;
};

template <class... Args>
static std::string format_string(const char* fmt, Args... args)
{
    const int BUF_SIZE = 1024;
    char buf[BUF_SIZE];
    std::snprintf(buf, BUF_SIZE, fmt, args...);
    return std::string(buf);
}

int main(int argc, char* argv[])
{
    argc = 3;
    argv[1] = "/home/chen/Learning/test_SGM/catkin_ws_stero_SGM/Left.jpg";
    argv[2] = "/home/chen/Learning/test_SGM/catkin_ws_stero_SGM/Right.jpg";
    if (argc < 3) {
        std::cout << "usage: " << argv[0] << " left-image-format right-image-format [disp_size]" << std::endl;
        std::exit(EXIT_FAILURE);
    }

    //ros


    const int first_frame = 1;

    //读入图片 这里是cv Mat格式
    cv::Mat I1 = cv::imread(format_string(argv[1], first_frame), -1);
    cv::Mat I2 = cv::imread(format_string(argv[2], first_frame), -1);
    const int disp_size = argc >= 4 ? std::stoi(argv[3]) : 128;
    //检查图像格式是否满足相关要求
    ASSERT_MSG(!I1.empty() && !I2.empty(), "imread failed.");
    ASSERT_MSG(I1.size() == I2.size() && I1.type() == I2.type(), "input images must be same size and type.");
    ASSERT_MSG(I1.type() == CV_8U || I1.type() == CV_16U, "input image format must be CV_8U or CV_16U.");
    ASSERT_MSG(disp_size == 64 || disp_size == 128 || disp_size == 256, "disparity size must be 64, 128 or 256.");

    //设置图像相关尺寸大小和数据大小
    const int width = I1.cols;
    const int height = I1.rows;

    const int input_depth = I1.type() == CV_8U ? 8 : 16;
    const int input_bytes = input_depth * width * height / 8;
    const int output_depth = disp_size < 256 ? 8 : 16;
    const int output_bytes = output_depth * width * height / 8;

    //初始化StereoSGM类
    //sgm(图片宽度，图片高度，视差最大值，输入图片位宽， 输出视差图位宽， )
    sgm::StereoSGM sgm(width, height, disp_size, input_depth, output_depth, sgm::EXECUTE_INOUT_CUDA2CUDA);

    const int invalid_disp = output_depth == 8
            ? static_cast< uint8_t>(sgm.get_invalid_disparity())
            : static_cast<uint16_t>(sgm.get_invalid_disparity());

    cv::Mat disparity(height, width, output_depth == 8 ? CV_8U : CV_16U);
    cv::Mat disparity_8u, disparity_color;
    //在CUDA中创建相关数据的buffer
    device_buffer d_I1(input_bytes), d_I2(input_bytes), d_disparity(output_bytes);

    for (int frame_no = first_frame;; frame_no++) {

        I1 = cv::imread(format_string(argv[1], frame_no), -1);
        I2 = cv::imread(format_string(argv[2], frame_no), -1);
        if (I1.empty() || I2.empty()) {
            frame_no = first_frame;
            continue;
        }
        //将图片数据从host复制到GPU中
        cudaMemcpy(d_I1.data, I1.data, input_bytes, cudaMemcpyHostToDevice);
        cudaMemcpy(d_I2.data, I2.data, input_bytes, cudaMemcpyHostToDevice);

        const auto t1 = std::chrono::system_clock::now();
        //在GPU中执行相关运算
        sgm.execute(d_I1.data, d_I2.data, d_disparity.data);
        cudaDeviceSynchronize();

        const auto t2 = std::chrono::system_clock::now();
        const auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
        const double fps = 1e6 / duration;
        //将结果从GPU复制到本地设备
        cudaMemcpy(disparity.data, d_disparity.data, output_bytes, cudaMemcpyDeviceToHost);

        // 绘制结果
        if (I1.type() != CV_8U) {
            cv::normalize(I1, I1, 0, 255, cv::NORM_MINMAX);
            I1.convertTo(I1, CV_8U);
        }

        disparity.convertTo(disparity_8u, CV_8U, 255. / disp_size);
        cv::applyColorMap(disparity_8u, disparity_color, cv::COLORMAP_JET);
        disparity_color.setTo(cv::Scalar(0, 0, 0), disparity == invalid_disp);
        cv::putText(disparity_color, format_string("sgm execution time: %4.1f[msec] %4.1f[FPS]", 1e-3 * duration, fps),
            cv::Point(50, 50), 2, 0.75, cv::Scalar(255, 255, 255));

        cv::imshow("left image", I1);
        cv::imshow("disparity", disparity_color);
        const char c = cv::waitKey(1);
        if (c == 27) // ESC
            break;
    }

    return 0;
}
