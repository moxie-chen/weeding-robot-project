#include <QCoreApplication>
#include <stereo_camera_ranging.h>

#include <zed_actuator_calibration.h>

int main(int argc, char *argv[])
{
    zed_actuator_calibration img_file("D:/temp/left","D:/temp/right");
    for(auto i:img_file.left_img_path_vec)
    {
        std::cout << i << std::endl;
    }
    for(auto j:img_file.right_img_path_vec)
    {
        std::cout << j << std::endl;
    }

    QCoreApplication a(argc, argv);

    return a.exec();
}
