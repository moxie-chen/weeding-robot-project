#ifndef ZED_ACTUATOR_CALIBRATION_H
#define ZED_ACTUATOR_CALIBRATION_H

#include "stereo_camera_ranging.h"
#include <io.h>

struct TRIGID_TRANS_3D
{
    double matR[9];

    double X;
    double Y;
    double Z;
};

class zed_actuator_calibration
{
public:
    zed_actuator_calibration(std::string, std::string);
    virtual ~zed_actuator_calibration();

    TRIGID_TRANS_3D getRigidTrans3D(std::vector<cv::Point3d> srcPoints, std::vector<cv::Point3d> dstPoints);

    void dirFindImg(const std::string,std::vector<std::string>&);

    std::vector<cv::Point3d> getWorldCoordinate(std::vector<std::string>, std::vector<std::string>);

    std::vector<std::string> left_img_path_vec, right_img_path_vec;
private:
    const std::string left_img_dir, right_img_dir;
};

#endif // ZED_ACTUATOR_CALIBRATION_H
